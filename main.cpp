#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <vector>
#include <fstream>
#include <stdexcept>
#include <termios.h>
//#include <cstdint>
#include <sys/ioctl.h>
#include <map>
#include <sstream>
#include <cstdlib>

using namespace std;

bool receiveSome(int from, /*const*/ char* data, size_t size);
bool sendSome(int to, char* data, size_t size);
std::vector<char> readFile(const char* path);



void proc_client(const int& fd);

struct ClientInfo
{
    int client_no;
};

map<int, ClientInfo> clients;

int main()
{
    cout << "Сервер!" << endl;

    int sock;
    int channel = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    sockaddr_in server;

    server.sin_family = AF_INET;

    cout << "Введите IP сервера: ";
    string address;
    cin >> address;

    server.sin_addr.s_addr = inet_addr(address.c_str());

    cout << "Введите порт: ";
    unsigned short port;
    cin >> port;
    server.sin_port = htons(port);
    cout << port;
	

    int yes;
    yes = 1;
    if (ioctl(channel, FIONBIO, &yes) == -1) {
		cout << "-1";
		printf("Error %d:%s\n", errno, strerror(errno));
		return -1;
    }

	
    int result = bind(channel, (const sockaddr*)(&server), sizeof(server));
    if (result != 0) {
	printf("Error %d:%s\n", errno, strerror(errno));
	return -1;
    }

    int listen_result = listen(channel, SOMAXCONN);
    if (listen_result != 0) {
	printf("Error %d:%s\n", errno, strerror(errno));
	return -1;
    }

    fd_set readset;

    int Number = 0;
    while (1) {

		FD_ZERO(&readset);
		FD_SET(channel, &readset);

		int maxfd = channel;
		for (map<int, ClientInfo>::iterator it = clients.begin(); it != clients.end(); it++) {
			const int& fd = it->first;
			FD_SET(fd, &readset);
			maxfd = fd > maxfd ? fd : maxfd;
		}
		++maxfd;
		timeval timeout;
		timeout.tv_sec = 30;
		timeout.tv_usec = 0;

		// Ждём события в одном из сокетов
		int ready_sock = select(maxfd, &readset, NULL, NULL, &timeout);
		std::cout << "\nСокеты, которые надо обслужить=" << ready_sock << std::endl;
		if (ready_sock == -1) {
			perror("Select");//префикс, где возникает ошибка
			exit(6);
		}

		if (FD_ISSET(channel, &readset)) {

			sockaddr_in peer;
			socklen_t peer_size = sizeof(peer);

			sock = accept(channel, (sockaddr*)&peer, &peer_size);

			cout << "Подключился клиент!Его адрес: " << inet_ntoa(peer.sin_addr) << endl;
			cout << "Его порт: " << ntohs(peer.sin_port) << endl;
			if (sock == -1) {
			perror("accept");
			break;
			}

			if (ioctl(sock, FIONBIO, &yes) == -1) {
				perror("ioctlsocket_sock");
				break;
			}

			++Number; // увеличиваем счетчик
					  // подключившихся клиентов
			clients.insert({ sock, { Number } });
		}

		for (map<int, ClientInfo>::iterator it = clients.begin(); it != clients.end(); it++) {
			const int& fd = it->first;
			if (FD_ISSET(fd, &readset)) {
				std::cout << "Дискриптор подключившегося клиента, fd=" << fd << std::endl;
				proc_client(fd);
			}
		}
    }
    return 0;
}

void proc_client(const int& fd)
{
    uint64_t msgSize = 0;

    if (!receiveSome(fd, (char*)&msgSize, sizeof(msgSize))){
		std::cout << "Проблемы с клиентом. Отключаем его." << std::endl;
		clients.erase(fd);
		close(fd);
		return;
	}
	
	//cout << "Msg size is: " << msgSize << endl;
	char msgType = '\0';

	if (receiveSome(fd, &msgType, sizeof(msgType))) {
	    cout << "Тип пакета: " << msgType << endl;
	    if (msgType == 'Q'){
			return;
		}

	    vector<char> msg;
	    msg.resize(msgSize + 1, '\0');

	    if (receiveSome(fd, msg.data(), msgSize)) {
		cout << "Тип: " << msgType << endl;
		string str(msg.begin(), msg.end());
		//cout << str << endl;
		cout << msg.data() << endl;

		try
		{
		    vector<char> content = readFile(msg.data());
		    uint64_t fileSize = content.size();
		    if (sendSome(fd, (char*)&fileSize, sizeof(fileSize))) {
			char msgType = 'F';
			if (sendSome(fd, &msgType, sizeof(msgType))) {
			    if (sendSome(fd, content.data(), fileSize)) {
				cout << "Файл успешно передан!\n";
			    } else {
				cout << "Ошибка передачи файла!\n";
				printf("Error %d:%s\n", errno, strerror(errno));
				return;
			    }
			} else {
			    cout << "Ошибка передачи типа!\n";
			    printf("Error %d:%s\n", errno, strerror(errno));
			    return;
			}
		    } else {
			cout << "Ошибка передачи размера!n";
			printf("Error %d:%s\n", errno, strerror(errno));
			return;
		    }
		}
		catch (const std::runtime_error& error)
		{
		    uint64_t msgSize = strlen(error.what());
		    if (sendSome(fd, (char*)&msgSize, sizeof(msgSize))) {
			char msgType = 'E';
			if (sendSome(fd, &msgType, sizeof(msgType))) {
			    if (sendSome(fd, (char*)error.what(), msgSize)) {
				cout << "Передано сообщение об ошибке!\n";
			    } else {
				cout << "Ошибка!\n";
				printf("Error %d:%s\n", errno, strerror(errno));
				return;
			    }
			} else {
			    cout << "Ошибка!\n";
			    printf("Error %d:%s\n", errno, strerror(errno));
			    return;
			}
		    } else {
			cout << "Ошибка!\n";
			printf("Error %d:%s\n", errno, strerror(errno));
			return;
		    }
		}
	    } else {
		cout << "Ошибка!\n";
		printf("Error %d:%s\n", errno, strerror(errno));
		return;
	    }
	}
    }


bool receiveSome(int from, /*const*/ char* data, size_t size)
{
    int received = 0;
    int bytes_received = 0;
    cout << "Нужно принять: " << size << endl << endl;
    while (bytes_received != size) {
		received = recv(from, /*(char*)*/ data, size - bytes_received, 0);
				
if (received == 0){
		std::cout << "Соединение прервано клиентом\n";
		return false;
		}
		if (received == -1) {
		printf("");
		continue;
		
		
		}
		bytes_received += received;
		cout << "Принято: " << bytes_received << endl;
		cout << "Осталось допринять: " << size - bytes_received << endl;
    }
    return true;
}

bool sendSome(int to, char* data, size_t size)
{
    int bytes_send = 0;
    while (bytes_send != size) {
	bytes_send = send(to, data, size, 0);
	if (bytes_send == -1) {
	    printf("Error %d:%s\n", errno, strerror(errno));
	    return false;
	}
	cout << "Отправлено: " << bytes_send << " bytes" << endl;
    }
    return true;
}

std::vector<char> readFile(const char* path)
{
    using namespace std;
    ifstream input(path, ios::in | ios::binary);
    if (!input) {
	throw runtime_error("File inaccessible!");
    }
    input.seekg(0, ios::end);
    int size = input.tellg();
    vector<char> content(size);
    input.seekg(0, ios::beg);
    input.read(content.data(), size);
    return content;
}